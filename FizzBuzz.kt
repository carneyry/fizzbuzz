import java.util.Scanner
fun main(args: Array<String>) {
    //creating Scanner object
    val read = Scanner(System.`in`)

    //Taking integer input
    println("Enter an integer number: ")
    var num1 = read.nextInt()
	if (num1%3==0 && num1%5==0){
        println("Fizzbuzz")
    }
    else if(num1%3==0 && num1%5!=0){
        println("Fizz")
    }
    else if(num1%5==0 && num1%3!=0){
        println("Buzz")
    }
    
}